package com.classification.lemmes.util;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Downloader {

    public static Document getDocument(String url) {// télécharger la page
        Document document = null;
        try {
            document = Jsoup.connect(url)
                    .ignoreContentType(true)
                    .userAgent("Mozilla")
                    .referrer("http://www.google.com")
                    .timeout(30000)
                    .followRedirects(true)
                    .execute().parse();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return document;
    }
}
