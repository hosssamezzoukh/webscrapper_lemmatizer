package com.classification.lemmes.util;

import com.classification.lemmes.io.XmlSaxParser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.SAXParserFactory;

public class Utils {

    static boolean hasArabicContent(Document document) {
        return document.select("html").first().attr("lang").equalsIgnoreCase("ar");
    }

    static String getFileName(String data) throws NoSuchAlgorithmException {
        return new BigInteger(1, MessageDigest.getInstance("MD5").digest(data.getBytes())).toString(16);
    }

    static Set<String> getUrlsDosument(Document document) {
        Set<String> urls = new HashSet<>();
        Elements elements = document.select("a[href]");
        for (Element element : elements) {
            String lien = element.attr("abs:href");
            if (lien.startsWith("https://ar.wikipedia.org/")
                    && !lien.contains("#")) {// ignorer les lien interne
                urls.add(lien);
            }
        }
        return urls;
    }

    public static void saveUrls(Set<String> ulrs) throws IOException {
        try (BufferedWriter buff = new BufferedWriter(new FileWriter(new File("Urls" + File.separator + "Urls.txt")))) {
            for (String url : ulrs) {
                buff.write(url + "\n");
            }
            buff.flush();
        }
    }

    public static void saveDocument(Document document, String url) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("Corpus" + File.separator + Utils.getFileName(url) + ".txt"))) {
            writer.write(document.body().text());
            writer.flush();
        } catch (IOException | NoSuchAlgorithmException e) {
        }
    }

    public static List<String> readStopWords() throws Exception {
        XmlSaxParser handler = new XmlSaxParser();
        SAXParserFactory.newInstance().newSAXParser().parse("UnvoweledPropernoun.xml", handler);
        SAXParserFactory.newInstance().newSAXParser().parse("UnvoweledToolwords.xml", handler);
        return handler.getStopWords();
    }
}
