package com.classification.lemmes.util;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.nodes.Document;

/**
 *
 * @author Souhail TOUNSI
 */
public class Crawler extends Thread {

    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(50);
    private static final Long MAX_PAGES = 100000L;
    private static final Set<Long> THREAD_IDS = new HashSet<>();
    // liste contient les liens des pages
    private static final Set<String> URLS = new HashSet<>();
    private static Long count = 1L;
    private static boolean stop = false;
    private static final long BEGIN;

    private final String url;

    static {
        BEGIN = System.currentTimeMillis();
        System.out.println("Crawler begin");
    }

    private Crawler(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        // arréter le thread si l'utilisateur a demandé d'arréter le service Crawler
        if (stop) {
            return;
        }
        // Démarrer le thread
        starting(this.getId());
        // Démarrer le téléchargement du document
        download();
        // arréter le thread ou le service Crawler s'il est terminée
        shutdown(this.getId());
    }

    private void download() {
        System.out.println("# Thread : " + this.getId() + " commencer à télécharger le document à partir de l'URL : " + url);
        // télécharger le document
        Document document = Downloader.getDocument(url);
        // accéder le bloc si le document à un contenu arabic
        if (Utils.hasArabicContent(document)) {
            // stocker le document dans le Corpus
            Utils.saveDocument(document, url);
            // sélectionner les urls de documents qui sont pointées par ce document
            for (String _url : Utils.getUrlsDosument(document)) {
                // en quite la boucle puis en quite le service crawler
                // si le nombre de pages est égal au maximum (100 000 pages),
                // ARRÊTEZ la boucle, puis le robot (Crawler)
                if (count >= MAX_PAGES) {
                    URLS.remove(_url);
                    break;
                }
                // exécuter un thread pour télécharger le documetn a partir d'url (_url)
                submit(_url);
            }
        } else {
            // le document contient du contenu non arabe
            System.err.println("ERROR : Document non arabe.");
        }
    }

    public static void start(String url) {
        submit(url);
    }

    static synchronized void starting(long id) {
        THREAD_IDS.add(id);
    }

    public static void stopping() {
        stop = true;
    }

    public static synchronized void shutdown(Long id) {
        if (id != null && THREAD_IDS.contains(id)) {
            THREAD_IDS.remove(id);
        }
        if (THREAD_IDS.isEmpty()) {
            try {
                Utils.saveUrls(URLS);
                EXECUTOR_SERVICE.shutdown();
                System.out.println("Crawler finished at " + (System.currentTimeMillis() - BEGIN) / 60000);
                System.out.println("size of Files : " + new File("Corpus").listFiles().length + " file");
            } catch (IOException e) {
            }
        }
    }

    private static boolean isTerminated() {
        return EXECUTOR_SERVICE.isTerminated();
    }

    public static synchronized void submit(String url) {
        if (!URLS.contains(url)) {
            URLS.add(url);
            count++;
            EXECUTOR_SERVICE.submit(new Crawler(url));
        }
    }
}
