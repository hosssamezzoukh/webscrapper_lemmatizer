package com.classification.lemmes.util;

import java.io.BufferedWriter;
import net.oujda_nlp_team.ADATAnalyzer;
import net.oujda_nlp_team.AlKhalil2Analyzer;
import net.oujda_nlp_team.util.Normalization;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Souhail TOUNSI
 */
public class Lemmatizer extends Thread {

    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(50);
    static final Set<Long> THREAD_IDS = new HashSet<>();
    static final Object LOCK_LEMMES = new Object();
    static final File[] FILES = new File("Corpus").listFiles();

    static final AlKhalil2Analyzer ALKHALIL_ANALYZER = new AlKhalil2Analyzer();
    static final ADATAnalyzer ANALYSER = new ADATAnalyzer();
    static final Normalization NORMALIZATION = new Normalization();
    static List<String> STOP_WORDS;

    static final List<String> LEMMES = new ArrayList<>();
    private static long BEGIN;
    private static boolean stop = false;

    static {
        BEGIN = System.currentTimeMillis();
        ALKHALIL_ANALYZER.addAllDataMap();
        try {
            STOP_WORDS = Utils.readStopWords();
        } catch (Exception e) {
        }
    }

    private final File file;

    private Lemmatizer(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        // arréter le thread si l'utilisateur a demandé d'arréter le service Lemmatizer
        if (stop) {
            return;
        }
        // Démarrer le thread
        starting(this.getId());
        // Démarrer le Lemmatization du document
        startLemmatization();
        // arréter le thread ou le service Lemmatizer s'il est terminée
        shutdown(this.getId());
    }

    public static void begin() {
        BEGIN = System.currentTimeMillis();
        if (FILES != null) {
            for (File file : FILES) {
                // exécuter un thread pour Lemmatizer le documetn file
                submit(file);
            }
        }
    }

    static synchronized void starting(long id) {
        THREAD_IDS.add(id);
    }

    private void startLemmatization() {
        System.out.println("#   Thread : " + this.getId() + " ,  Filename : " + file.getName());

        try {
            List<String> listMot = (List<String>) NORMALIZATION.NormalizationFileToList(file, "UTF-8", false);
            List<String> mots = new ArrayList<>();
            // filtrer la liste des mots en eliminer les mots qui sont une longeur inf ou égal à 2
//            for (String mot : listMot) {
//                if (mot.length() > 2) {
//                    mots.add(mot);
//                }
////            }
//            mots.removeAll(STOP_WORDS);
            String textLemmatizer = ANALYSER.processLemmatizer2(String.join(" ", listMot), ALKHALIL_ANALYZER);
            // récuperer les lemmes à partir de test Lemmatizer
            List<String> lemmes = getLemmes(textLemmatizer);
            // ajouter les lemmes du current document au total LEMMES
            addLemmes(lemmes);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private List<String> getLemmes(String text) {
        List<String> lemmes = new ArrayList<>();
        String[] motAndLemme = text.split(" ");
        for (String mot : motAndLemme) {
            mot = mot.trim();
            if (mot.length() > 1) {
                String[] split = mot.split(":");
                if (split.length == 2 && !split[1].equals("-")) {
                    lemmes.add(split[1]);
                }
            }
        }
        return lemmes;
    }

    static synchronized void addLemmes(List<String> lemmes) {
        LEMMES.addAll(lemmes);
    }

    public static void stopping() {
        stop = true;
    }

    public static synchronized void shutdown(Long id) {
        if (id != null && THREAD_IDS.contains(id)) {
            THREAD_IDS.remove(id);
        }
        if (THREAD_IDS.isEmpty()) {
            TreeMap<String, Long> map = new TreeMap<>();
            for (String lemme : LEMMES) {
                map.put(lemme, !map.containsKey(lemme) ? 1 : map.get(lemme) + 1);
            }
            long end = System.currentTimeMillis();

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File("LemmesAndStopWordFound" + File.separator + "Lemmes.txt")))) {
                for (Map.Entry<String, Long> lemme : map.entrySet()) {
                    writer.append(lemme.getKey()).append(":").append(lemme.getValue() + "").append("\n");
                }
                writer.flush();
            } catch (IOException ex) {
                Logger.getLogger(Lemmatizer.class.getName()).log(Level.SEVERE, null, ex);
            }

            
            System.out.println("Limmatizer finished at " + (end - BEGIN) / 60000 + "min");
            System.out.println("size of lemmes : " + map.size() + " lemme");
            //System.out.println("size of stop words : " + stopWords);
            System.out.println("Time seconds : " + (end - BEGIN) / 1000);
            EXECUTOR_SERVICE.shutdown();
        }
    }

    static boolean isTerminated() {
        return EXECUTOR_SERVICE.isTerminated();
    }

    static void submit(File file) {
        EXECUTOR_SERVICE.submit(new Lemmatizer(file));
    }
}
