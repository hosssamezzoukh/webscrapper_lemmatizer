package com.classification.lemmes.io;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;


public class XmlSaxParser extends DefaultHandler {

    private static final List<String> stopWords = new ArrayList<>();

    public List<String> getStopWords() {
        return stopWords;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        stopWords.add(attributes.getValue("unvoweledform"));
    }
}
