package com.classification.lemmes.app;

import com.classification.lemmes.util.Crawler;
import com.classification.lemmes.util.Lemmatizer;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 *
 * @author Souhail TOUNSI
 */
public class ClassificationLemmes {

    static final String url = "https://ar.wikipedia.org/";
    //static final String file = "/home/hunter/Bureau/html/index.html";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        Scanner in = new Scanner(System.in);
        int mode = -1;
        System.out.println("---------------- Choisir le mode d'éxécution (1 ou 2) ----------------");
        do {
            System.out.println("Entrée 0 - Pour quiter le programme.");
            System.out.println("Entrée 1 - (éxécution de Crawler pour Construre le Corpus) pour Télécharger les données à partir d'URL : " + url);
            System.out.println("Entrée 2 - (éxécution de Lemmatizer) pour la Lemmatization des données de Corpus.");
            System.out.println("  - Choix : ");
            if (in.hasNextInt()) {
                mode = in.nextInt();
            }
        } while (mode != 1 && mode != 2 && mode != 0);

        switch (mode) {
            case 1:
                Crawler.submit(url);
                do {
                    System.out.println("########################### Téléchargement les données à partir d'internet ##############################");
                    System.out.println("Entrée 0 - Pour quiter le programme.");
                    System.out.println("Entrée 1 - (éxécution de Lemmatizer) pour la Lemmatization des données de Corpus.");
                    System.out.print("  - Choix : ");
                    if (in.hasNextInt()) {
                        mode = in.nextInt();
                    }
                } while (mode != 1 && mode != 0);
                Crawler.stopping();
                if (mode == 1) {
                    Lemmatizer.begin();
                    do {
                        System.out.println("########################### Lemmatization les données à partir de corpus ##############################");
                        System.out.println("Entrée 0 - Pour quiter le programme.");
                        System.out.print("  - Choix : ");
                        if (in.hasNextInt()) {
                            mode = in.nextInt();
                        }
                    } while (mode != 0);
                    Lemmatizer.stopping();
                }
                break;
            case 2:
                Lemmatizer.begin();
                do {
                    System.out.println("########################### Lemmatization les données à partir de corpus ##############################");
                    System.out.println("Entrée 0 - Pour quiter le programme.");
                    System.out.print("  - Choix : ");
                    if (in.hasNextInt()) {
                        mode = in.nextInt();
                    }
                } while (mode != 0);
                Lemmatizer.stopping();
                break;
            default:
                System.exit(0);
        }
        System.out.println("Wait.......................................");
        Crawler.stopping();
        Lemmatizer.stopping();
    }
}
